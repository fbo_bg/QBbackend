﻿using System;
using System.Data.Common;
using System.Data.SQLite;

namespace Backend.DataStorage
{
    public class SqliteDbManager : IDbManager
    {
        public DbConnection GetConnection()
        {
            try
            {
                var connection = new SQLiteConnection($"Data Source={AppDomain.CurrentDomain.BaseDirectory}\\citystatecountry.db;Version=3;FailIfMissing=True");
                return connection.OpenAndReturn();
            }
            catch(SQLiteException ex)
            {
                Console.WriteLine("Failed to get connection. " + ex.Message);
                throw;
            }
        }
    }
}