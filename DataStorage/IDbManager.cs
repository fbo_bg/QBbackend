﻿using System.Data.Common;

namespace Backend.DataStorage
{
    public interface IDbManager
    {
        DbConnection GetConnection();
    }
}
