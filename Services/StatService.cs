﻿using Backend.Clients;
using Backend.DataAccess;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class StatService : IStatService
    {
        private readonly IStatRepo statRepo;
        private readonly IStatApiClient statApiClient;

        public StatService(IStatRepo statRepo, IStatApiClient statApiClient)
        {
            this.statRepo = statRepo;
            this.statApiClient = statApiClient;
        }

        public async Task<IList<(string, int)>> GetCountryPopulationsFromApiAndRepoAsync(IMergeStrategy mergeStrategy)
        {
            var statsFromRepo = statRepo.GetCountryPopulationsAsync();
            var statsFromApi = statApiClient.GetCountryPopulationsAsync();

            var result = mergeStrategy.Merge(await statsFromRepo, await statsFromApi);

            return result;
        }
    }
}