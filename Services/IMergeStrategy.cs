﻿using System;
using System.Collections.Generic;

namespace Backend.Services
{
    public interface IMergeStrategy
    {
        /// <summary>
        /// Merge two tuple lists
        /// </summary>
        IList<(string, int)> Merge(IList<(string, int)> dbCountryPopulations, IList<Tuple<string, int>> apiCountryPopulations);
    }
}
