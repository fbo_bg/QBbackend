﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Services
{
    interface IStatService
    {
        Task<IList<(string, int)>> GetCountryPopulationsFromApiAndRepoAsync(IMergeStrategy mergeStrategy);
    }
}
