﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Services
{
    /// <summary>
    /// Merges two tuple lists by creating a dictionary from the first list, mapping the created keys if needed, and adding the missing mapped pairs to the dictionary
    /// </summary>
    public class MergeStrategy : IMergeStrategy
    {
        private readonly IDictionary<string, string> countryMappings;

        public MergeStrategy(IDictionary<string, string> countryMappings)
        {
            this.countryMappings = countryMappings ?? new Dictionary<string, string>();
        }

        public IList<(string, int)> Merge(IList<(string, int)> dbCountryPopulations, IList<Tuple<string, int>> apiCountryPopulations)
        {

            dbCountryPopulations = dbCountryPopulations ?? new List<(string, int)>();
            apiCountryPopulations = apiCountryPopulations ?? new List<Tuple<string, int>>();

            var mergedCountryPopulations = dbCountryPopulations.ToDictionary(cp => cp.Item1, cp => cp.Item2);

            if (apiCountryPopulations.Count > 0)
            { 
                foreach (var (country, population) in apiCountryPopulations)
                {
                    // apply mapping to key from tuple
                    if (!countryMappings.TryGetValue(country, out var countryAfterMapping))
                    {
                        countryAfterMapping = country;
                    }

                    // add key value pair if missing
                    if (!mergedCountryPopulations.ContainsKey(countryAfterMapping))
                    {
                        mergedCountryPopulations.Add(countryAfterMapping, population);
                    }
                }
            }

            return mergedCountryPopulations.Select(cp => (cp.Key, cp.Value)).ToList();
        }
    }
}