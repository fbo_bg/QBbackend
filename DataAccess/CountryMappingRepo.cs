﻿using System.Collections.Generic;

namespace Backend.DataAccess
{
    class CountryMappingRepo : ICountryMappingRepo
    {
        private readonly Dictionary<string, string> countryMappings = new Dictionary<string, string>
            {{"United States of America", "U.S.A."}};

        public Dictionary<string, string> GetCountryMapping()
        {
            return countryMappings;
        }
    }
}