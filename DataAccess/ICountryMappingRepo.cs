﻿using System.Collections.Generic;

namespace Backend.DataAccess
{
    interface ICountryMappingRepo
    {
        Dictionary<string, string> GetCountryMapping();
    }
}
