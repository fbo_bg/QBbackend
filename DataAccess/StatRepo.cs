﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Backend.DataStorage;

namespace Backend.DataAccess 
{
    public class StatRepo : IStatRepo
    {
        private readonly IDbManager db;

        public StatRepo(IDbManager db)
        {
            this.db = db;
        }
        
        public async Task<IList<(string, int)>> GetCountryPopulationsAsync()
        {
            Console.WriteLine("Getting DB Connection...");
            DbConnection conn = db.GetConnection();

            var countriesPopulations = new List<(string, int)>();
            try
            {
                using (var command = conn.CreateCommand())
                {
                    // take care of nulls from sum as well with having clause
                    command.CommandText =
                    @"
                        SELECT 
                            [Country].[CountryName] AS CountryName, 
                            SUM([City].[Population]) AS Population 
                        FROM [Country] 
                        JOIN [State] ON[Country].[CountryId] = [State].[CountryId] 
                        JOIN [City] ON[State].[StateId] = [City].[CityId] 
                        GROUP BY [Country].[CountryName]
                        HAVING SUM([City].[Population]) IS NOT NULL
                    ";

                    var reader = await command.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        // sqlite returns Int64, so a conversion to Int32 is needed
                        countriesPopulations.Add((reader["CountryName"].ToString(), Convert.ToInt32(reader["Population"])));
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("An exception occured when trying to read countries populations from the db. " + e.Message);
                throw;
            }
            finally
            {
                conn.Dispose();
            }            
            
            return countriesPopulations;
        }
    }
}