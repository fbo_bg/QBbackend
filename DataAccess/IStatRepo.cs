﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.DataAccess
{
    public interface IStatRepo
    {
        Task<IList<(string, int)>> GetCountryPopulationsAsync();
    }
}