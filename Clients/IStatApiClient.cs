﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Clients
{
    public interface IStatApiClient
    {
        List<Tuple<string, int>> GetCountryPopulations();
        Task<List<Tuple<string, int>>> GetCountryPopulationsAsync();
    }
}
