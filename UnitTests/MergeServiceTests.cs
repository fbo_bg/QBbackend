﻿using Backend.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace UnitTests
{
    [TestFixture]
    public class MergeServiceTests
    {
        [Test]
        public void ShouldMergeCountriesPopulationsWithMapping()
        {
            // Arrange
            var primary = new List<(string, int)>
            {
                ( "India", 1182105000 ),
                ( "Germany", 80000000 ),
                ( "United States", 40000000 )
            };

            var secondary = new List<Tuple<string, int>>
            {
                Tuple.Create("India", 100),
                Tuple.Create("United Kingdom", 62026962),
                Tuple.Create("US", 1000)
            };

            var expectedResult = new List<(string, int)>
            {
                ("India", 1182105000),
                ("Germany", 80000000),
                ("United States", 40000000),
                ("United Kingdom", 62026962)
            };

            var mergeService = new MergeStrategy(new Dictionary<string, string>{{"US", "United States"}});

            // Act
            var actualResult = mergeService.Merge(primary, secondary);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldMergeCountriesPopulationsWithoutMapping()
        {
            // Arrange
            var primary = new List<(string, int)>
            {
                ( "India", 1182105000 ),
                ( "Germany", 80000000 )
            };

            var secondary = new List<Tuple<string, int>>
            {
                Tuple.Create("India", 100),
                Tuple.Create("United Kingdom", 62026962)
            };

            var expectedResult = new List<(string, int)>
            {
                ("India", 1182105000),
                ("Germany", 80000000),
                ("United Kingdom", 62026962)
            };

            var mergeService = new MergeStrategy(null);

            // Act
            var actualResult = mergeService.Merge(primary, secondary);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldMergeCountriesPopulationsWithEmptyMapping()
        {
            // Arrange
            var primary = new List<(string, int)>
            {
                ( "India", 1182105000 ),
                ( "Germany", 80000000 )
            };

            var secondary = new List<Tuple<string, int>>
            {
                Tuple.Create("India", 100),
                Tuple.Create("United Kingdom", 62026962)
            };

            var expectedResult = new List<(string, int)>
            {
                ("India", 1182105000),
                ("Germany", 80000000),
                ("United Kingdom", 62026962)
            };

            var mergeService = new MergeStrategy(null);

            // Act
            var actualResult = mergeService.Merge(primary, secondary);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnPrimarySetWhenSecondaryIsNull()
        {
            // Arrange
            var primary = new List<(string, int)>
            {
                ( "India", 1182105000 ),
                ( "Germany", 80000000 )
            };
            

            var expectedResult = new List<(string, int)>
            {
                ("India", 1182105000),
                ("Germany", 80000000)
            };

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(primary, null);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnSecondarySetWhenPrimaryIsNull()
        {
            // Arrange
            var secondary = new List<Tuple<string, int>>
            {
                Tuple.Create("India", 100),
                Tuple.Create("United Kingdom", 62026962)
            };

            var expectedResult = new List<(string, int)>
            {
                ("India", 100),
                ("United Kingdom", 62026962)
            };

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(null, secondary);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnEmptySetWhenPrimaryIsNullAndSecondaryIsNull()
        {
            // Arrange
            var expectedResult = new List<(string, int)>();

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(null, null);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnPrimarySetWhenSecondaryIsEmpty()
        {
            // Arrange
            var primary = new List<(string, int)>
            {
                ( "India", 1182105000 ),
                ( "Germany", 80000000 )
            };


            var expectedResult = new List<(string, int)>
            {
                ("India", 1182105000),
                ("Germany", 80000000)
            };

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(primary, new List<Tuple<string, int>>());

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnSecondarySetWhenPrimaryIsEmpty()
        {
            // Arrange
            var secondary = new List<Tuple<string, int>>
            {
                Tuple.Create("India", 100),
                Tuple.Create("United Kingdom", 62026962)
            };

            var expectedResult = new List<(string, int)>
            {
                ("India", 100),
                ("United Kingdom", 62026962)
            };

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(new List<(string, int)>(), secondary);

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }

        [Test]
        public void ShouldReturnEmptySetWhenPrimaryIsEmptyAndSecondaryIsEmpty()
        {
            // Arrange
            var expectedResult = new List<(string, int)>();

            var mergeService = new MergeStrategy(new Dictionary<string, string>());

            // Act
            var actualResult = mergeService.Merge(new List<(string, int)>(), new List<Tuple<string, int>>());

            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }


    }
}