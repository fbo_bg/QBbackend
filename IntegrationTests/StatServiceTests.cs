﻿using Backend.Clients;
using Backend.DataAccess;
using Backend.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IntegrationTests
{
    [TestFixture]
    public class StatServiceTests
    {
        [Test]
        public async Task ShouldReturnCountriesPopulationsFromRepoAndApi()
        {
            // Arrange
            var mockStatRepo = new Mock<IStatRepo>();
            mockStatRepo.Setup(x => x.GetCountryPopulationsAsync()).ReturnsAsync(new List<(string, int)>
            {
                ("Japan", 10000)
            });

            var mockStatApiClient = new Mock<IStatApiClient>();
            mockStatApiClient.Setup(x => x.GetCountryPopulationsAsync()).ReturnsAsync(new List<Tuple<string, int>>
            {
                new Tuple<string, int>("Germany", 80),
                new Tuple<string, int>("Japan", 30)
            });

            var expectedResult = new List<(string, int)>
            {
                ("Japan", 10000),
                ("Germany", 80)
            };

            var statService = new StatService(mockStatRepo.Object, mockStatApiClient.Object);


            // Act
            var actualResult = await statService.GetCountryPopulationsFromApiAndRepoAsync(new MergeStrategy(null));


            // Assert
            CollectionAssert.AreEquivalent(expectedResult, actualResult);
        }
    }
}