﻿using Backend.DataAccess;
using Backend.DataStorage;
using NUnit.Framework;
using System.Threading.Tasks;

namespace IntegrationTests
{
    [TestFixture]
    public class StatRepoTests
    {
        [Test]
        public async Task ShouldGetAtLeastOneCountriesPopulationsFromRepo()
        {
            // Arrange
            var statRepo = new StatRepo(new SqliteDbManager());

            // Act
            var countriesPopulations = await statRepo.GetCountryPopulationsAsync();

            // Assert
            Assert.IsTrue(countriesPopulations.Count > 0);
        }
    }
}
