﻿using Autofac;
using Backend.DataAccess;
using Backend.Services;
using System;

namespace Backend
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // merge data
                var container = IoCContainerFactory.BuildIoCContainer();
                var statService = container.Resolve<IStatService>();
                var countryMapping = container.Resolve<ICountryMappingRepo>().GetCountryMapping();
                var countriesPopulations = statService.GetCountryPopulationsFromApiAndRepoAsync(new MergeStrategy(countryMapping)).Result;

                // log output
                foreach (var countryPopulation in countriesPopulations)
                {
                    Console.WriteLine($"{countryPopulation.Item1}    {countryPopulation.Item2}");
                }

                Console.ReadLine();
            }
            catch(AggregateException ae)
            {
                Console.WriteLine("An error(e) occured when running the program. ");
                foreach (var e in ae.Flatten().InnerExceptions)
                {
                   Console.WriteLine(e.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured when running the program. " + e.Message);
            }

            // keep console open
            Console.ReadLine();
        }
    }
}