﻿using Autofac;
using Backend.Clients;
using Backend.DataAccess;
using Backend.DataStorage;
using Backend.Services;

namespace Backend
{
    public static class IoCContainerFactory
    {
        public static IContainer BuildIoCContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<SqliteDbManager>().As<IDbManager>();
            builder.RegisterType<StatRepo>().As<IStatRepo>();
            builder.RegisterType<StatApiClient>().As<IStatApiClient>();
            builder.RegisterType<StatService>().As<IStatService>();
            builder.RegisterType<CountryMappingRepo>().As<ICountryMappingRepo>();
            return builder.Build();
        }
    }
}
